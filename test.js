"use strict";

// Set Proxy
//const ip2asn = new (require('./src/ip2asn/index.js'))({'url': 'http://127.0.0.1/'});
const ip2asn = new (require('./src/ip2asn/index.js'));

// ip2asn.get('8.8.8.8')
// .then(data => {

//     console.log("Status        : " + data.status);
//     console.log("Error         : " + data.error);
//     console.log("IP Address    : " + data.ipAddress);
//     console.log("IP Version    : " + data.ipVersion);
//     data.announcedBy.forEach(asn => {

//         console.log("Announced By  : " +  JSON.stringify(asn));

//     })

// })

ip2asn.getPrimary('8.8.8.8').then(response => console.log(response));